# Downloads HTML (after JavaScript execution) from Turkish energy website https://ytbs.teias.gov.tr/ytbs/frm_login.jsf
# Saves resulting HTML as a file.
#
# Note: The data on the Turkish website is in the Turkish timezone (GMT+3)
#
# Probably want to run turkish_scrape.pl after this to extract the data to CSV files
#
# Written by: Wes Cox
# Aug 2, 2018
#
# usage: python3 ./download_html.py <start date (YYYY-MM-DD)> <end date (YYYY-MM-DD)



from selenium import webdriver
from time import sleep
import sys, os
from datetime import datetime, timedelta
from pathlib import Path

html_dir = './HTML/'

# Get the start and stop dates from the command line parameters
if len(sys.argv) != 3:
	sys.exit("usage: python3 " + str(os.path.basename(__file__)) + " <start date (YYYY-MM-DD)> <end date (YYYY-MM-DD)>")

start_date_str = sys.argv[1]
end_date_str = sys.argv[2]

start_date = None
end_date = None

dates = []

try:
	start_date = datetime.strptime(start_date_str, "%Y-%m-%d").date()
except ValueError:
	sys.exit("Error: Not a valid start date: '{0}'".format(start_date_str))

try:
	end_date = datetime.strptime(end_date_str, "%Y-%m-%d").date()
except ValueError:
	sys.exit("Error: Not a valid end date: '{0}'".format(end_date_str))
	
if end_date < start_date:
	sys.exit("Error: Start date must be less than or equal to end date")

# Create a directory for all the downloaded html files (if it doesn't exist)
if not os.path.exists(html_dir):
	os.makedirs(html_dir)

# Create a list of all the dates between the start and end dates
delta = end_date - start_date

for i in range(delta.days + 1):
	dates.append((start_date + timedelta(i)).strftime("%Y-%m-%d"))

# Load up the webpage in Chrome
driver = webdriver.Chrome(str(Path.home()) + "/bin/chromedriver")
sleep(1) # Give it time to start ChromeDriver

driver.get("https://ytbs.teias.gov.tr/ytbs/frm_login.jsf")

# Iterate over all the dates and save the corresponding html

for date_of_interest in dates:
	print("Downloading data for : " + str(date_of_interest))
	date_input = driver.find_element_by_id('formdash:bitisTarihi2_input')
	submit_button = driver.find_element_by_id('formdash:j_idt17')
	date_input.clear()
	date_input.send_keys(date_of_interest)
	submit_button.click()

	page = driver.page_source
	output_file = open(html_dir + 'YTBS_' + str(date_of_interest) + '.html','w', encoding="utf-8")
	output_file.write(page)
	output_file.close()
	sleep(2)

sleep(5)
driver.quit()
print("All done")