This code downloads (Java executed) HTML files from a website tracking the Turkish energy industry (https://ytbs.teias.gov.tr/ytbs/frm_login.jsf) and extracts the data to CSV files.

Python3 is used to download the pages as it interfaces with Selenium more easily. Perl is used for the actual scraping. 

Requires: 

* chromedriver to be installed to allow Chrome to be run headlessly. (http://chromedriver.chromium.org/)

* selenium package installed in python3 (pip3 install selenium)

Other than that, just a standard Perl/Python3 environment.
