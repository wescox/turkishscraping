#!/usr/bin/perl -w

# Extracts the data from the graphs plotted on Turkish energy website https://ytbs.teias.gov.tr/ytbs/frm_login.jsf
# Saves resulting data as CSV files
#
# Note: The data on the Turkish website is in the Turkish timezone (GMT+3)
#
# Requires: download_html.py to be run prior to this
#
# Written by: Wes Cox
# Aug 2, 2018
#
# usage: perl ./turkish_scrape.pl <start date (YYYY-MM-DD)> <end date (YYYY-MM-DD)



# Take input date range
use DateTime;

my $histogram_filename = 'histogram';
my $pie_filename = 'pie';
my $chart_filename = 'chart';
my @data;
my $handle;
my %dates;

$html_dir = './HTML/';
$data_dir = './Data/';

# Read in command line parameters
@ARGV == 2 or die "usage: perl $0 <start date (YYYY-MM-DD)> <end date (YYYY-MM-DD)>\n";

$start_date = shift @ARGV;
$end_date = shift @ARGV;

$start_date =~ m/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/ or die "Invalid start date $start_date\n";
$end_date =~ m/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/ or die "Invalid end date $start_date\n";

# Make sure start <= end
my ($start_y, $start_m, $start_d) = split /-/, $start_date;
eval{ $start_date_obj = DateTime->new(year => $start_y, month => $start_m, day => $start_d) or die "Invalid start date\n"; };
if ($@) {
	die "Start date object creation failed: $@\n";
}

my ($end_y, $end_m, $end_d) = split /-/, $end_date;
eval { $end_date_obj = DateTime->new(year => $end_y, month => $end_m, day => $end_d) or die "Invalid end date\n"; };
if ($@) {
	die "End date object creation failed: $@\n";
}

if (DateTime->compare($start_date_obj, $end_date_obj) >= 0) {
	die "Start date must be less than or equal to the end date\n";
}

# Create dictionary of dates between start and end dates
my $curr = $start_date_obj;
while ($curr <= $end_date_obj) {
	$dates{$curr->ymd} = $curr;
	$curr->add(days => 1);
}

# Create directory to store output data
if (! -e $data_dir) {
	mkdir($data_dir) or die "Can't create directory $data_dir: $!\n";
}

# Iterate through the HTML directory and process any files for the dates between start and end
opendir(DIR, $html_dir) or die "Cannot open directory $html_dir: $!\n";

while ( ($filename = readdir(DIR))) {

	$filename =~ /.*_([0-9]{4}\-[0-9]{2}\-[0-9]{2})\.html/;
	my $date_str = $1;

	if ($filename eq '.' or $filename eq '..' or ($filename !~ m/YTBS_([0-9]{4}\-[0-9]{2}\-[0-9]{2})\.html/) or ! exists $dates{$1}) {
		# File is not in our date range, ignore it
		next;
	}

	print "Processing: $filename";

	OpenFile($html_dir . $filename);
	print ".";
	ExtractHistogramData($date_str);
	print ".";
	ExtractPieData($date_str);
	print ".";
	ExtractChartData($date_str);
	print " done\n";

	# Remove this date from the list of dates, so we can make sure they all got done
	delete $dates{$date_str}
}

if (%dates) {
	print "The following dates found no corresponding HTML files to process:\n";
	foreach my $key (keys %dates) {
		print "$key\n";
	}
	die;
}
else {
	print "All done\n";
}

sub OpenFile {
	open $handle, "<$_[0]" or die;
	@lines = <$handle>;

	@lines = grep { $_ =~ /aria\-label/ } @lines;

	for (@lines) {
		s/^\s+//gm; # Remove leading whitespace
		s/\s+$//gm; # Remove trailing whitespace
		s/(aria\-label=\"[^\"]*\")/$1\n/gm; # put new lines after the values
		s/.*(aria\-label=\"[^\"]*\").*/$1/gm; # Remove anything excess from aria-label lines

		s/^((?!aria\-label=).)*$//gm; # Remove anything that doesnt start with aria-label

		if (/^aria\-label=/) { # Make sure the line starts with aria-label
			push (@data, (split /\n/, $_)); # Break up any strings containing newlines and store in new array
		}
	}
}

sub ExtractHistogramData {
	$date = $_[0] or die "Invalid date parameter\n";
	open (my $fh_histogram, '>', $data_dir . $histogram_filename . '_' . $date . '.csv') or die "Could not open file '$histogram_filename' $!\n";
	for (@data) {
		chomp;
		if (/^aria\-label=\"\ .*/) { # Only look at the histogram data which all have a space before the data
			s/aria\-label=\"\ (.*)\".*/$1/g;
			s/\ /,/g;
			print $fh_histogram "$_\n";
		} 
	}
	close $fh_histogram;
}

sub ExtractPieData {
	$date = $_[0] or die "Invalid date parameter\n";
	open (my $fh_pie, '>', $data_dir . $pie_filename . '_' . $date . '.csv') or die "Could not open file '$pie_filename' $!\n";
	for (@data) {
		chomp;
		if (/:/) { # Only look at the pie data which all have a colon in the data
			s/aria\-label=\"(.*)\".*/$1/g; # Extract just the data

			s/\s+$//gm; # Remove trailing whitespace

			@row = split /:/; # Want to modify the data without the labels

			$row[1] =~ s/\.//g; # Get rid of the thousands indicator
			$row[1] =~ s/,/\./g; # Swap the decimal and commas. Damn europeans.

			$row[1] =~ s/\ /,/g; # Now separate the data ready for CSV

			$row[0] =~ s/://g; # Get rid of colon

			$_ = join "", @row; # Combine back into a single row

			print $fh_pie "$_\n";
		} 
	}
	close $fh_pie;
}

sub ExtractChartData {
	$date = $_[0] or die "Invalid date parameter\n";
	seek $handle, 0, 0;
	@html = <$handle>;

	my @html_data;
	my $index;

	for (@html) {
		s/^\s+//gm; # Remove leading whitespace
		s/\s+$//gm; # Remove trailing whitespace
		if (!/var gunlukUretimEgrisiData =/) { # We are only interested in the data in the gunlukUretimEgrisiData variable
			next;
		}

		s/^.*\[(.*)\].*$/$1/g; # Just get the data in the square brackets

		s/\},/\}\n/g; # Break up the data into rows

		s/[{}]*//gm; #Drop all the curly braces

		push (@html_data, (split /\n/, $_)); # Break up any strings containing newlines and store in new array
	}
	close $handle;

	open (my $fh_chart, '>', $data_dir . $chart_filename . '_' . $date . '.csv') or die "Could not open file '$chart_filename' $!\n";
	for (@html_data) {

		# Only look in the first line for the labels
		if ($index++ eq 0) {
			$first = $_;
			$first =~ s/\":[^\"]*,\"/,/g; # Drop everything except the label names

			$first =~ s/^\"//g; # Drop the leading garbage
			$first =~ s/\".*$//g; # Drop the trailing garbage

			print $fh_chart "$first\n";
		}

		s/\"[^\"]*\"://g; # Drop the labels, and just give me the data

		print $fh_chart "$_\n";
	}
	close $fh_chart;
}

